// const user = 'john';
// const age = 32;
// const email = 'john@example.com';
// const door_no = 34;
// const apartment = 'sunshine villa';
// const street = 'james street';
// const city = 'New York';
// const country = 'United States';

// function greetUser() {
//     console.log('Hello ' + user);
// }

// // function as variable
// const greetUser = function () {
//     console.log('Hello ' + user);
// }

// greetUser();
const name = "foo";
const myuser = {
  // fields -> key-value pairs
  name: "John",
  age: 30,
  email: "john@example.com",
  address: {
    door_no: 30,
    apartment: "sunshine villa",
    street: "jane street",
    city: "New York",
    country: "United states"
  },
  greet: function() {
    console.log("Hello " + this.name);
  }
};

console.log(user.greet());
