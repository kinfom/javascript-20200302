const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14];
const othernumbers = [15, 16, 17, 18, 19, 20, 21, 22, 23];
//const newnumbers = numbers; // not copying old array, you are just copying the location (mutate old array)
let newnumbers = [...numbers, ...othernumbers]; // spread operator, makes old array immutable
// newnumbers.push(15);
newnumbers = [...newnumbers, 24, 25, 26];
newnumbers = [...numbers];
console.log(newnumbers);
console.log(numbers);

let a = 5;
let b = a; // copies only value
b = 6;
console.log(a, b);

// redux - react;
