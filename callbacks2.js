function add(x, y) {
  setTimeout(() => {
    console.log("add called..");
    console.log(x + y);
  }, 3000);
}
function sub(x, y) {
  setTimeout(() => {
    console.log("sub called..");
    console.log(x - y);
  }, 3000);
}

function calculate(a, b, operation1, operation2) {
  operation1(a, b);
  operation2(a, b);
  console.log("result printed..");
}

calculate(3, 4, sub, add);

// $('#btn').click(function(){})
// calculate called
// call to calculate function
// calculate calls add
// add prints add called..
// add prints 7
// calculate prints result printed
