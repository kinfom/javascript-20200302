// let promise = new Promise(function(resolve, reject) {
//   const a = 4;
//   //   const b = 0;
//   const c = a / b;
//   console.log(c);
//   c ? resolve("function resolved") : reject("function rejected");
// });

// promise.then(res => console.log(res)).catch(err => console.log(err));

// pending - new
// resolve - completed
// reject - error
// let today = new Date();

// console.log(today.getMilliseconds());

// setInterval(() => {
//   let today = new Date();
//   console.log(today.getMilliseconds());
// }, 1);

const haveMoney = true;

const possessEnoughMoney = new Promise((resolve, reject) => {
  if (haveMoney) {
    var food = {
      lunch: "pizza",
      drink: "coke"
    };
    resolve(food);
  } else {
    var food = new Error("no money...no food");
    reject(food);
  }
});

const eatSomething = function() {
  possessEnoughMoney
    .then(function(eat) {
      console.log(eat);
    })
    .catch(function(starve) {
      console.log(starve);
    });
};

eatSomething();
