var numbers = [1, 2, 3, 4, 5];
var filtered = numbers.filter(number => number !== 3);
console.log(filtered);

var users = [
  {
    id: 1,
    name: "john"
  },
  {
    id: 2,
    name: "alex"
  },
  { id: 3, name: "joe" }
];
var filteredusers = users.filter(user => user.id != 2);
console.log(filteredusers);
