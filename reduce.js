const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14];

// let total = 0;

// numbers.forEach((number, index) => {
//   total = total + number;
//   console.log(index);
// });

// console.log(total);

let result = numbers.reduce((total, number) => {
  number = Math.sqrt(number);
  return total + number;
}, 0);
console.log(result);
