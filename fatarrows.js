// 1990's

function printName() {
  console.log("john");
}

// 2000's

const printName = function() {
  console.log("john");
};

// 2020's

const printName = () => console.log("john");

const printName = () => console.log("john"); // fat arrow notation es6
