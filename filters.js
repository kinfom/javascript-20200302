const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];
const evens = numbers.filter(x => x % 2 == 0);
const odds = numbers.filter(number => number % 2 != 0);

// numbers.forEach(number => {
//   //   if (number % 2 == 0) {
//   //     evens.push(number);
//   //   } else {
//   //     odds.push(number);
//   //   }

//   number % 2 == 0 ? evens.push(number) : odds.push(number);
// });
console.log(evens, odds);
