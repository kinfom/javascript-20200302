var Rectangle = /** @class */ (function () {
    // is the default method called as soon as the object is created
    function Rectangle(x, y) {
        console.log("constructor called");
        this.length = x;
        this.breadth = y;
    }
    // print details
    Rectangle.prototype.printDetails = function () {
        console.log(this.name + " is a rectangle with length as " + this.length + " units and breadth as " + this.breadth + " units");
    };
    return Rectangle;
}());
var rect = new Rectangle(10, 20); // creating a new object/instance
// rect.length = 34;
// rect.breadth = 56;
rect.name = "my rectangle";
rect.printDetails();
var rect2 = new Rectangle(30, 40); // creating a new object/instance
// rect.length = 21;
// rect.breadth = 67;
rect2.name = "my second rectangle";
rect2.printDetails();
