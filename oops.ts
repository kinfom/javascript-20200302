class Rectangle {
  // data members
  name: string;
  length: number;
  breadth: number;
  // is the default method called as soon as the object is created
  constructor(x, y) {
    console.log("constructor called");
    this.length = 2*x;
    this.breadth = 2*y;
  }
  // print details
  printDetails() {
    console.log(
      `${this.name} is a rectangle with length as ${this.length} units and breadth as ${this.breadth} units`
    );
  }
}
let rect = new Rectangle(10, 20); // creating a new object/instance
// rect.length = 34;
// rect.breadth = 56;
rect.name = "my rectangle";
rect.printDetails();

let rect2 = new Rectangle(30, 40); // creating a new object/instance
// rect.length = 21;
// rect.breadth = 67;
rect2.name = "my second rectangle";
rect2.printDetails();
