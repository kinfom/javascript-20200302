const users = [
  {
    name: "john",
    age: 30,
    email: "john@example.com"
  },
  {
    name: "alex",
    age: 33,
    email: "alex@example.com"
  },
  {
    name: "emi",
    age: 20,
    email: "emi@example.com"
  }
];

let [x, , z] = users; // destructuring the array

console.log(z.name);
