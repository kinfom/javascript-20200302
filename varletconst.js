// THIS IS A SINGLE LINE COMMENTATION
/*
COMMENT 1
COMMENT 2
COMMENT 3
.....
..


ES 6 

VAR - ES5 - VARIABLES - FUNCTION SCOPE
LET - ES 6 - VARIABLES - BLOCK SCOPE
CONST - ES 6 - CONSTANT - BLOCK SCOPE

*/

const name = "john";
function printName() {
  var type = "human";
  var canTalk = "I dont know";
  if (type == "human") {
    let canTalk = true;
    let dumb = true;
    if (dumb) {
      canTalk = false;
    }
    console.log(name + " is a " + type, canTalk); // canTalk = true
  } else {
    let canTalk = false;
    console.log(name + "is not a " + type, canTalk); // not executed, runs only if type is not human
  }
  console.log(canTalk); // I dont know
}
console.log(name);
printName();
